resource "aws_instance" "r100c96" {
  ami               = "ami-03a0c45ebc70f98ea"
  instance_type     = "t2.micro"
  availability_zone = "us-east-2a"
  key_name          = "bitbuck"
  security_groups = ["${aws_security_group.Ts.name}"]
  tags = {
    Name = "master"
  }

  provisioner "local-exec" {
    command = "chmod 400 bitbuck.pem"
  }

  provisioner "local-exec" {
    command = "echo '[master]\n' ${aws_instance.r100c96.public_ip} >> inventory"
  }


  provisioner "remote-exec" {
    inline = [ "sudo hostnamectl set-hostname cloudEc2.technix.com" ]
    connection {
      host        = aws_instance.r100c96.public_dns
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("bitbuck.pem")
    }
  }




  provisioner "local-exec" {
    command = "ansible all --list-hosts"
  }
  provisioner "local-exec" {
    command = "ansible-playbook  nginx.yml --private-key bitbuck.pem -e 'ansible_python_interpreter=/usr/bin/python3' "
  }
}
